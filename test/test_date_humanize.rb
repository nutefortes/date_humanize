require 'minitest_helper'
require 'date_humanize'

class TestDateHumanize < Minitest::Test
  def test_humanize_days
    assert_equal "1 dia",                                      DateHumanize.humanize_days(1)
    assert_equal "2 dias",                                     DateHumanize.humanize_days(2)
    assert_equal "1 semana",                                   DateHumanize.humanize_days(7)
    assert_equal "14 dias",                                    DateHumanize.humanize_days(14)
    assert_equal "29 dias",                                    DateHumanize.humanize_days(29)
    assert_equal "30 dias (aprox. 1 mês)",                     DateHumanize.humanize_days(30)
    assert_equal "31 dias (aprox. 1 mês)",                     DateHumanize.humanize_days(31)
    assert_equal "32 dias",                                    DateHumanize.humanize_days(32)
    assert_equal "45 dias",                                    DateHumanize.humanize_days(45)
    assert_equal "60 dias (aprox. 2 meses)",                   DateHumanize.humanize_days(60)
    assert_equal "61 dias (aprox. 2 meses)",                   DateHumanize.humanize_days(61)
    assert_equal "62 dias",                                    DateHumanize.humanize_days(62)
    assert_equal "70 dias",                                    DateHumanize.humanize_days(70)
    assert_equal "90 dias (aprox. 3 meses)",                   DateHumanize.humanize_days(90)
    assert_equal "91 dias (aprox. 3 meses)",                   DateHumanize.humanize_days(91)
    assert_equal "92 dias (aprox. 3 meses)",                   DateHumanize.humanize_days(92)
    assert_equal "93 dias",                                    DateHumanize.humanize_days(93)
    assert_equal "120 dias (aprox. 4 meses)",                  DateHumanize.humanize_days(120)
    assert_equal "121 dias (aprox. 4 meses)",                  DateHumanize.humanize_days(121)
    assert_equal "122 dias (aprox. 4 meses)",                  DateHumanize.humanize_days(122)
    assert_equal "123 dias",                                   DateHumanize.humanize_days(123)
    assert_equal "150 dias (aprox. 5 meses)",                  DateHumanize.humanize_days(150)
    assert_equal "151 dias (aprox. 5 meses)",                  DateHumanize.humanize_days(151)
    assert_equal "152 dias (aprox. 5 meses)",                  DateHumanize.humanize_days(152)
    assert_equal "153 dias (aprox. 5 meses)",                  DateHumanize.humanize_days(153)
    assert_equal "154 dias",                                   DateHumanize.humanize_days(154)
    assert_equal "179 dias",                                   DateHumanize.humanize_days(179)
    assert_equal "180 dias (aprox. 6 meses)",                  DateHumanize.humanize_days(180)
    assert_equal "181 dias (aprox. 6 meses)",                  DateHumanize.humanize_days(181)
    assert_equal "182 dias (aprox. 6 meses)",                  DateHumanize.humanize_days(182)
    assert_equal "183 dias (aprox. 6 meses)",                  DateHumanize.humanize_days(183)
    assert_equal "184 dias",                                   DateHumanize.humanize_days(184)
    assert_equal "210 dias (aprox. 7 meses)",                  DateHumanize.humanize_days(210)
    assert_equal "211 dias (aprox. 7 meses)",                  DateHumanize.humanize_days(211)
    assert_equal "212 dias (aprox. 7 meses)",                  DateHumanize.humanize_days(212)
    assert_equal "213 dias (aprox. 7 meses)",                  DateHumanize.humanize_days(213)
    assert_equal "214 dias (aprox. 7 meses)",                  DateHumanize.humanize_days(214)
    assert_equal "215 dias",                                   DateHumanize.humanize_days(215)
    assert_equal "240 dias (aprox. 8 meses)",                  DateHumanize.humanize_days(240)
    assert_equal "241 dias (aprox. 8 meses)",                  DateHumanize.humanize_days(241)
    assert_equal "242 dias (aprox. 8 meses)",                  DateHumanize.humanize_days(242)
    assert_equal "243 dias (aprox. 8 meses)",                  DateHumanize.humanize_days(243)
    assert_equal "244 dias (aprox. 8 meses)",                  DateHumanize.humanize_days(244)
    assert_equal "245 dias",                                   DateHumanize.humanize_days(245)
    assert_equal "270 dias (aprox. 9 meses)",                  DateHumanize.humanize_days(270)
    assert_equal "271 dias (aprox. 9 meses)",                  DateHumanize.humanize_days(271)
    assert_equal "272 dias (aprox. 9 meses)",                  DateHumanize.humanize_days(272)
    assert_equal "273 dias (aprox. 9 meses)",                  DateHumanize.humanize_days(273)
    assert_equal "274 dias (aprox. 9 meses)",                  DateHumanize.humanize_days(274)
    assert_equal "275 dias (aprox. 9 meses)",                  DateHumanize.humanize_days(275)
    assert_equal "276 dias",                                   DateHumanize.humanize_days(276)
    assert_equal "300 dias (aprox. 10 meses)",                 DateHumanize.humanize_days(300)
    assert_equal "301 dias (aprox. 10 meses)",                 DateHumanize.humanize_days(301)
    assert_equal "302 dias (aprox. 10 meses)",                 DateHumanize.humanize_days(302)
    assert_equal "303 dias (aprox. 10 meses)",                 DateHumanize.humanize_days(303)
    assert_equal "304 dias (aprox. 10 meses)",                 DateHumanize.humanize_days(304)
    assert_equal "305 dias (aprox. 10 meses)",                 DateHumanize.humanize_days(305)
    assert_equal "306 dias",                                   DateHumanize.humanize_days(306)
    assert_equal "330 dias (aprox. 11 meses)",                 DateHumanize.humanize_days(330)
    assert_equal "331 dias (aprox. 11 meses)",                 DateHumanize.humanize_days(331)
    assert_equal "332 dias (aprox. 11 meses)",                 DateHumanize.humanize_days(332)
    assert_equal "333 dias (aprox. 11 meses)",                 DateHumanize.humanize_days(333)
    assert_equal "334 dias (aprox. 11 meses)",                 DateHumanize.humanize_days(334)
    assert_equal "335 dias (aprox. 11 meses)",                 DateHumanize.humanize_days(335)
    assert_equal "336 dias (aprox. 11 meses)",                 DateHumanize.humanize_days(336)
    assert_equal "337 dias",                                   DateHumanize.humanize_days(337)
    assert_equal "1 ano",                                      DateHumanize.humanize_days(365)
    assert_equal "10 anos",                                    DateHumanize.humanize_days(3650)
    assert_equal "1 ano e 1 dia",                              DateHumanize.humanize_days(366)
    assert_equal "1 ano e 1 semana",                           DateHumanize.humanize_days(372)
    assert_equal "1 ano e 20 dias",                            DateHumanize.humanize_days(385)
    assert_equal "1 ano e 30 dias (aprox. 1 ano e 1 mês)",     DateHumanize.humanize_days(395)
    assert_equal "1 ano e 182 dias (aprox. 1 ano e 6 meses)",  DateHumanize.humanize_days(547)
    assert_equal "1 ano e 331 dias (aprox. 1 ano e 11 meses)", DateHumanize.humanize_days(696)
    assert_equal "10 anos e 1 dia",                            DateHumanize.humanize_days(3651)
    assert_equal "10 anos e 30 dias (aprox. 10 anos e 1 mês)", DateHumanize.humanize_days(3680)
  end
end
