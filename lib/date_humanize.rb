require "date_humanize/version"
require 'action_view'

include ActionView::Helpers::TextHelper

module DateHumanize
  def self.humanize_days days
    years = days/365

    if years > 0
      days -= years*365
    end

    months = days/30

    "#{print_days days, years}#{print_aprox days, months, years}"
  end

  def self.print_days days, years
    return "#{prefix_years(years)}1 semana" if days == 7
    return pluralize_years years if days%365 == 0
    return "#{prefix_years(years)}#{pluralize_days days}"
  end

  def self.print_aprox days, months, years
    return "" unless show_aprox? days, months
    return " (aprox. #{aprox_value months, years})"
  end

  def self.show_aprox? days, months
    return false if days < 30
    return false if days%365 == 0
    return false if days > limit_days_to_show_aprox(months)

    return true
  end

  def self.prefix_years years
    "#{pluralize_years years} e " if years > 0
  end

  def self.aprox_value months, years
    "#{prefix_years years}#{pluralize_months months}"
  end

  def self.limit_days_to_show_aprox months
    (months*30) + (months.to_f/2).round
  end

  def self.pluralize_days days
    "#{pluralize(days, 'dia', 'dias')}"
  end

  def self.pluralize_months months
    "#{pluralize(months, 'mês', 'meses')}"
  end

  def self.pluralize_years years
    "#{pluralize(years, 'ano', 'anos')}"
  end
end
